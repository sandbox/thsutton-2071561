<?php
/**
 * @file
 * Administer settings to restrict the full display of nodes.
 */

/**
 * FAPI constructor: configure the module settings.
 */
function restrict_node_display_config($form, $form_state) {

  $form['introduction'] = array(
    '#markup' => t('<p>Set the default configuration for node display restrictions below. You will need to configure the settings for your <a href="!url">content types</a> for these to take effect.</p>', array(
      '!url' => url('admin/structure/types'),
    )),
    '#weight' => -9999,
  );

  $form['restrict_node_display_default_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable default'),
    '#description' => t('Enable restrictions by default?'),
    '#default_value' => variable_get('restrict_node_display_default_enabled', 0),
  );

  $view_modes = _restrict_node_display_get_types();
  $form['restrict_node_display_default_view_mode'] = array(
    '#type' => 'select',
    '#title' => t('Private view mode default'),
    '#description' => t('Select the default view mode to display to un-privileged users.'),
    '#required' => TRUE,
    '#options' => $view_modes,
    '#default_value' => variable_get('restrict_node_display_default_view_mode', 'teaser'),
  );

  $form['restrict_node_display_default_login_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display login form default'),
    '#description' => t('Display the login form when displaying the private view mode by default?'),
    '#default_value' => variable_get('restrict_node_display_default_login_form', 0),
  );

  return system_settings_form($form);
}
