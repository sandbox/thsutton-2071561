(function ($) {

Drupal.behaviors.restrict_node_display = {
  attach: function (context) {
    // Provide the vertical tab summaries.
    $('fieldset#edit-restrict-node-display-settings', context).drupalSetSummary(function(context) {
      var enabled, login, vals = [];
      enabled = $('input:radio[name="restrict_node_display_enabled"]:checked').val();
      vals.push(enabled == 1 ? Drupal.t('Enabled') : Drupal.t('Disabled'));

      if (enabled == 1) {
        vals.push(Drupal.t('Display: !view_mode', {'!view_mode' : $('#edit-restrict-node-display-view-mode option:selected').text()}));

        login = $('input:radio[name="restrict_node_display_login_form"]:checked').val();
        if (login == 1) {
          vals.push(Drupal.t('with login form'));
        }
      }

      return vals.join(', ');
    });

  }
};

})(jQuery);
