Restrict Node Display
=====================

This module allows the site administrator to override the "Full" view mode with
another, selected, view mode unless the viewing user has a specific permission.
The expected use-case is to display a "teaser" instead of the "full" node when
viewed by an anonymous user. 

This module was sponsored by [Digital Garden Web Design Sydney][1].

[1]: http://www.digitalgarden.com.au/


Installation
------------

Install this module using the normal Drupal [module installation procedure][2].

[2]: https://drupal.org/documentation/install/modules-themes/modules-7


Configuration
-------------

The first step is to configure one or more content types to be restricted:

1. Navigate to Administration / Content Authoring / Restrict node display on
   your Drupal site.

2. Change the options to suit your needs.

3. Click the "Save configuration" form.

4. Navigate to Administration / Structure / Content types on your Drupal site.

5. Click the "edit" link for the content type you want to restrict.

6. Click the "Restrict display settings" field set.

7. Click the "Yes" radio button under "Enable".

8. Select the view mode to display instead of "Full content" to users who do 
   *not* have the special permission (more on this below). You might like to
   start off by selecting "Teaser" initially.

9. Select "Yes" if you want to display a login form when an anonymous user
   views a restricted node. **Please note** this functionality is not yet
   implemented.

10. Save the node type.

The second step is to grant permission to view the "Full content" to one or
more roles:

11. Navigate to Administration / People / Permissions.

12. Scroll down to the "Restrict Node Display" section of the form.

13. Tick the "View restricted nodes in the full view mode" checkbox for the
    appropriate roles.

14. Save the form.

Test your configuration:

15. Log out or open another web browser.

16. Navigate to a node of the type you edited in step (2) above.

17. You should see the node displayed in the view mode you selected at step (5)
    above.

18. Login with an account which has the "View restricted nodes in the full 
    view mode" permission. Either an account with the role/s you configured
    in step (10) above or the site administrator user.

19. You should see the node displayed in the "Full content" view mode.
