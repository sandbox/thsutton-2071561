<?php
/**
 * @file
 * Tests for the Restrict Node Display module.
 */

/**
 * Base class for Restrict Node Display test cases.
 */
class RestrictNodeDisplayBaseTestCase extends DrupalWebTestCase {
  protected $adminUser;
  protected $viewUser;
  protected $restrictedUser;
  protected $contentType;

  /**
   * Initialise environment for Restrict Node Display test cases.
   */
  public function setUp() {
    parent::setUp(array('restrict_node_display'));

    variable_set('restrict_node_display_debugging', TRUE);

    // Create a content type.
    $this->contentType = $this->drupalCreateContentType(array(
      'restrict_node_display_enabled' => 1,
      'restrict_node_display_view_mode' => 'teaser',
      'restrict_node_display_login_form' => 0,
    ));
    variable_set("restrict_node_display_enabled_{$this->contentType->type}", TRUE);
    variable_set("restrict_node_display_login_form_{$this->contentType->type}", FALSE);
    variable_set("restrict_node_display_view_mode_{$this->contentType->type}", 'teaser');

    // Create users.
    $this->adminUser = $this->drupalCreateUser(array(
      'restrict node display admin',
      'restrict node display view full',
    ));

    $this->viewUser = $this->drupalCreateUser(array(
      'restrict node display view full',
    ));

    $this->restrictedUser = $this->drupalCreateUser(array(
      'access content',
    ));
  }
}

/**
 * Tests functionality for configuring restrictions for content types.
 */
class RestrictNodeDisplayNodeTestCase extends RestrictNodeDisplayBaseTestCase {
  protected $restrictedNode;
  protected $unrestrictedNode;

  /**
   * Test case information.
   */
  public static function getInfo() {
    return array(
      'name' => 'Node CRUD',
      'description' => 'Test saving and loading of restrictions for nodes.',
      'group' => 'Restrict Node Display',
    );
  }

  /**
   * Initialise environment for Restrict Node Display test cases.
   */
  public function setUp() {
    parent::setUp();

    // Create $restricted_node.
    $this->restrictedNode = $this->drupalCreateNode(array(
      'type' => $this->contentType->type,
      'title' => $this->randomName(32),
      'restrict_node_display' => 1,
    ));

    // Create $unrestricted_node.
    $this->unrestrictedNode = $this->drupalCreateNode(array(
      'type' => $this->contentType->type,
      'title' => $this->randomName(32),
      'restrict_node_display' => 0,
    ));
  }

  /**
   * Test that nodes can be created w/ restriction settings.
   */
  public function testCreateNode() {
    // Load the unrestricted node and check that it's unrestricted.
    $node = node_load($this->unrestrictedNode->nid, NULL, TRUE);
    $restricted = isset($node->restrict_node_display) && $node->restrict_node_display;
    $this->assert(!$restricted, "Unrestricted node is unrestricted.");

    // Load the restricted node and check that it's restricted.
    $node = node_load($this->restrictedNode->nid, NULL, TRUE);
    $restricted = isset($node->restrict_node_display) && $node->restrict_node_display;
    $this->assert($restricted, "Restricted node is restricted.");
  }

  /**
   * Test that the restriction of a node can be changed.
   */
  public function testUpdateNode() {
    // 1. Create unrestricted node.
    // 2. Load and change it to restricted.
    // 3. Save it.
    // 4. Check it is restricted.
    //
    // 1. Create restricted node.
    // 2. Load and change it to unrestricted.
    // 3. Save it.
    // 4. Check it is unrestricted.
  }

  /**
   * Test that restriction is removed for deleted nodes.
   */
  public function testDeleteNode() {
    // 1. Create restricted node.
    // 2. Delete restricted node.
    // 3. Check the restriction table is empty.
  }
}

/**
 * Tests functionality for configuring restrictions for content types.
 */
class RestrictNodeDisplayRenderTestCase extends RestrictNodeDisplayBaseTestCase {
  protected $restrictedNode;
  protected $unrestrictedNode;

  /**
   * Test case information.
   */
  public static function getInfo() {
    return array(
      'name' => 'Node rendering',
      'description' => 'Test rendering of restricted nodes.',
      'group' => 'Restrict Node Display',
    );
  }

  /**
   * Initialise environment for Restrict Node Display test cases.
   */
  public function setUp() {
    parent::setUp();

    // Create $restricted_node.
    $this->restrictedNode = $this->drupalCreateNode(array(
      'type' => $this->contentType->type,
      'title' => $this->randomName(32),
      'restrict_node_display' => 1,
    ));

    // Create $unrestricted_node.
    $this->unrestrictedNode = $this->drupalCreateNode(array(
      'type' => $this->contentType->type,
      'title' => $this->randomName(32),
      'restrict_node_display' => 0,
    ));
  }

  /**
   * Test rendering restricted nodes.
   */
  public function testRenderRestrictedUser() {
    // Log in as $restrictedUser.
    $this->drupalLogin($this->restrictedUser);

    // View $restricted_node and check that it's using full.
    $this->drupalGet("node/{$this->restrictedNode->nid}");
    $this->assertText("This node is restricted.", "Unprivileged user can view full restricted content.");

    // View $unrestricted_node and check that it's using full.
    $this->drupalGet("node/{$this->unrestrictedNode->nid}");
    $this->assertNoText("This node is restricted.", "Unprivileged user can view full unrestricted content.");
  }

  /**
   * Test rendering unrestricted nodes.
   */
  public function testRenderUnrestrictedUser() {
    // Log in as $viewUser.
    $this->drupalLogin($this->viewUser);

    // View $restricted_node and check that it's using teaser.
    $this->drupalGet("node/{$this->restrictedNode->nid}");
    $this->assertNoText("This node is restricted.", "Privileged user cannot view full unrestricted content.");

    // View $unrestricted_node check that it's using full.
    $this->drupalGet("node/{$this->unrestrictedNode->nid}");
    $this->assertNoText("This node is restricted.", "Privileged user can view full unrestricted content.");
  }
}
